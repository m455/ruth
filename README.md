# ruth

An IRC bot in Racket. For more information about Ruth's commands, check out
[Ruth's wiki page on tilde.town](http://tilde.town/wiki/socializing/irc/bots/ruth.html).
